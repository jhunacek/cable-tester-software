\documentclass[10pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage[T1]{fontenc}
\usepackage{rotating}
\usepackage{hyperref}

\begin{document}

\title{Automated Cable Tester}
\author{Jonathon Hunacek}
\date{Updated 2018-01-24}
\maketitle

\section{Overview}

\begin{figure}
\begin{center}
\includegraphics[%
  width=0.95\linewidth,
  keepaspectratio]{img/mega-cable-tester.jpg}
\end{center}
\caption{The Mega Cable Tester main board.}
\label{fig_mainboard}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[%
  width=0.95\linewidth,
  keepaspectratio]{img/cable-tester.jpg}
\includegraphics[%
  width=0.95\linewidth,
  keepaspectratio]{img/adapters.jpg}
\end{center}
\caption{The Mini Cable Tester main board and adapter modules.}
\label{fig_mainboard}
\end{figure}

This document briefly describes the Mega Cable Tester, the Mini Cable Tester, and software used to operate them.  They allow measurement of the full cross-short matrix for a set of pins (128 for the mini, 724 for the mega), useful for checking cryostats and modules for unexpected shorts and opens.  Resistances up to $\sim$1 M$\Omega$ have been tested successfully.

\subsection{Circuit Overview}

A PDF of the circuit schematics for both hardware revisions is included with this document.

\subsubsection{Mega Cable Tester}

The Mega Cable Tester includes 5 100p MDM connectors, 6 37s MDM connectors, and 2 banana jacks for a total of 724 user-accessible pins.  Each pin is independently connected to two ADG732 analog multiplexer trees; one tree connects to ground through a 1 k$\Omega$ resistor, and the other to a fixed 2.5V through a 2 M$\Omega$ resistor (limiting the current to 1.3 $\mu$A).  An ADS1220 24 bit ADC with 1x-128x PGA is configured to optionally use the voltage over the 2 M$\Omega$ resistor as its reference voltage, allowing ratiometric measurement of the unknown resistor when current is flowing.  A Teensy 3.6 (Arduino software compatible ARM processor) communicates with the ADC and multiplexer chips and reports results to the PC via USB.

\subsubsection{Mini Cable Tester}

The Mini Cable Tester (formerly Cable Tester) has two sets of 128 connections, labeled Connector 0 and Connector 1.  ADG732 analog multiplexer chips allow the output of a LM234 constant current source to be connected to any single pin from Connector 0 or Connector 1.  Another set of analog multiplexer chips allows any single pin on Connector 0 to be connected to ground (connector 1 cannot be connected to ground; this ground multiplexer can also be disconnected to check for ground shorts).  An ADS1120 16 bit ADC measures the voltage drop over the two chosen pins (one connected to the current source, and one connected to ground).  An Arduino Zero communicates with the ADC and multiplexer chips and reports results to the PC via USB.  A digitally programmable potentiometer allows adjusting the current at run time - currently a single value is hardcoded into the firmware.

\subsection{Firmware Functional Description}

An Arduino sketch waits for a start signal sent over a serial connection by a computer.  The sketch then reads in parameters to set the operating mode and steps through each pin combination reporting the voltage or resistance read.  The readings are repeated at higher gains for combinations that are not out-of-range at lower gains.  All measurements are reported over serial as they are made, with the lasted reported measurement for each pin combination being the most accurate.

\section{Operation}

\texttt{cable-tester.py} is a python script that initiates measurements and records data to a spreadsheet.  Here are a couple of examples:\\

List and describe all possible arguments (read this!):\\
\texttt{./cable-tester.py -h}\\

Run the default configuration for the hardware (no pin mapping file):\\
\texttt{./cable-tester.py}\\

Probe out a 5 MDM MCE interface:\\
\texttt{./cable-tester.py -p ./pinmap/mega-mce-mux11.txt}\\

Check for cross shorts within one 100 pin MDM on the Mega Cable Tester with no pinmap:\\
\texttt{./cable-tester.py -a 1}\\

Pin mapping files allow one to configure pin names, measurement parameters, and pin resistance/voltage expectations.  These expectations are checked and reported in the first tab of the output file; subsequent tabs contain the full measurement matrix.  Valid commands are as follows:

\begin{itemize}

\item \texttt{hw [mega,mini]}\\
The hardware revision this pin mapping is for.

\item \texttt{j[a,b] [0-6]}\\
Number of JA/JB connectors used (mega only).  The first N connectors of each type are tested, others are skipped.

\item \texttt{mode [half,full,block-half,block-full,cross]}\\
Cable tester run mode.  Block modes (mega only) measure a block-diagonal matrix, only checking combinations within connectors.  Cross mode (mini only) checks from port 0 to port 1 instead of the default port 0 to port 0.

\item \texttt{name [pin\_id] [string]}\\
Assign the name \texttt{string} to pin \texttt{pin\_id}.  Use double quotes around names with spaces.  The order of assigned names in the pinmap file determines the order presented in the output file.  Valid pin IDs for the mini tester are GND and 001-128; for the mega tester, they are GND, EXT[0,1], A[0-4]:[001-100], B[0-5]:[001-037], and CAL[614-631]. 

\item \texttt{res [pin\_a] [pin\_b] [val\_min] [val\_max]}\\
Specify that a resistor is connected between pins \texttt{pin\_a} and \texttt{pin\_b}.  The software with check that the resistance is in the range [\texttt{val\_min}, \texttt{val\_max}]; all pin combinations without a \texttt{res} command specified have a default expectation of +inf.  SI prefixes may be used if desired (ex. ``1.3k'', ``4M''), as well as ``+inf'' or ``-inf''.

\item \texttt{diode [pin\_a] [pin\_b] [val\_min] [val\_max]}\\
\texttt{diode [pin\_a] [pin\_b] [val\_min] [val\_max] [reverse\_val\_min] [reverse\_val\_max]}\\
Specify that a diode is connected between pins \texttt{pin\_a} and \texttt{pin\_b}, where the cathode is connected to \texttt{pin\_a}.  The software with check that the voltage is in the range [\texttt{val\_min}, \texttt{val\_max}].  SI prefixes may be used if desired (ex. ``300m''), as well as ``+inf'' or ``-inf''.  Optionally, reverse voltage limits can be specified for diodes with large leakage current; otherwise, the reverse voltage expectation is set to +inf.

\end{itemize}



\subsection{Calibration}

Before starting measurements, both the mini and mega cable testers short the two mux outputs to guarantee that current is flowing.  They measure the voltage drop over a known fixed 1 k$\Omega$ series resistor and from that compute the current, which is assumed to be constant for all following measurements (the current is not used when computing resistance on the Mega Cable Tester, since measurements are ratiometric).  They also measure the voltage across the ``unknown'' resistance, which we have chosen to be 0 $\Omega$ (ideally) by shorting the outputs.  This allows us to measure the resistance of the mux chips themselves, which is on the order of 5 $\Omega$ each.  The value measured is assumed to be the same for all channels.

A series of known resistors are mounted on the board and connected to unused pins.  These can be used to verify the calibration.  See the schematic for details.

\subsection{Timing}

\subsubsection{Mega}
The mega cable tester can be limited to different subsets of connectors, affecting total run time.  The 500x500 half-matrix (A to B but not B to A, sufficient for resistors only) for all 5 100p MDMs completes in approximately 2 minutes.  The half-matrix for a single 100p MDM can be computed in about 10 seconds.

\subsubsection{Mini}
The full matrix (128x128) can be measured in $\sim$40 sec and a half-matrix in $\sim$20 sec.

\subsection{Safety}

The cable tester has been tested on actual SSA and SQ1 connections successfully.

The MCE IB checker from UBC uses an external multimeter for its resistance measurements.  I have verified that the multimeter is using a 5 $\mu$A constant current source with a max voltage of $\sim$7V.  I did this by running the IB checker LabView script with the multimeter output connected to dummy resistors and observing the voltage over the dummy resistor.  The Mini Cable Tester is using a 1 $\mu$A constant current source with a max voltage of $\sim$3.3V and 1 k$\Omega$ series resistor; the Mega Cable Tester uses a fixed 2.5V with a 2 M$\Omega$ series resistance (max 1.3 $\mu$A).  A 1 k$\Omega$ resistor is in series with the ground point connected to the multiplexer on both hardware revisions.

\section{Repositories}

Everything related to the cable tester is in git repos on BitBucket, so feel free to improve or replace the software.\newline


PC-side software:

\url{https://bitbucket.org/jhunacek/cable-tester-software}\newline


Firmware:

\url{https://bitbucket.org/jhunacek/mega-cable-tester-firmware}

\url{https://bitbucket.org/jhunacek/cable-tester-firmware}\newline


PCB drawings:

\url{https://bitbucket.org/jhunacek/cable-tester-pcb}


\end{document}


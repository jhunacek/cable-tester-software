# Cable Tester Control Software #

### Dependencies ###

* [Python](https://www.python.org/downloads/) (tested on v3.8.10)
* [numpy](https://pypi.python.org/pypi/numpy) (tested on v1.21.1)
* [openpyxl 2.x](https://openpyxl.readthedocs.io/en/stable/) (tested on v2.3.5)
* [pyserial](https://pyserial.readthedocs.io/en/latest/pyserial.html) (tested on v3.2.1)

Depedencies can be installed via [pip](https://packaging.python.org/tutorials/installing-packages/), either in a virtualenv or globally (as shown below):

`sudo pip3 install numpy~=1.21 openpyxl~=2.3 pyserial~=3.2`

On Linux systems, you must install the Teensy udev rules to use the Mega Cable Tester:

`wget https://www.pjrc.com/teensy/00-teensy.rules`

`sudo cp 00-teensy.rules /etc/udev/rules.d/`

`sudo udevadm control --reload-rules`

`sudo udevadm trigger`

On Linux systems, add yourself to the `dialout` group to avoid having to execute the cable tester software with `sudo`:

`sudo adduser $USER dialout`

### Usage ###

Execute `./cable-tester.py --help` from the terminal for usage instructions.  Further information can be found in the `docs` folder.

On Windows you will have to specify the serial port (see the help output); the default `/dev/ttyACM0` is only valid on Unix-based systems.  You can find the correct port number in the Windows Device Manager.

### Troubleshooting and Tips ###

As always, try disconnecting/reconnecting the device, restarting the terminal, or rebooting the system.

If you consistently get an error about `/dev/ttyACM0` being busy, and the issue persists after rebooting, it may be that `modemmanager` is taking control of the device.  Try removing it (`sudo apt-get purge modemmanager`).

Running and passing the `mega-calib.txt` pinmap (for the mega tester) with no cables plugged in is a good way to check the system is functioning (it checks on-board calibration resistors).

While not necessary, I recommend connecting your device/cryostat case ground to EXT0 instead of directly to the cable tester GND port, as hard shorts to the cable tester current source ground can make it hard to measure cross shorts on that pin until the fault is cleared.  USB isolators can be helpful in certain scenarios, as can running from a laptop on battery power.  Note that the metal case of the Mega Tester is grounded as well, so don't let grounded connector housings rest against the case if you are trying to keep ground floating.  Connecting to the cable tester ground will not hide any issues on pins without ground shorts - if you pass all checks while connected, you have passed.  Shorts to the cable tester ground are detected successfully, but create excess ghosts that look like a similar short to every other pin in one polarity.  In half matrix mode the Mega Tester chooses the polarity to minimize impacts of ground short ghosts, but they will be visible in full matrix mode.

Remember that, like a multimeter, the cable tester is injecting a single fixed DC current (~1 uA) and measuring the voltage drop, which is then interpeted as a resistance (or diode) by the software.  Therefore, like a multimeter, if you are measuring a complex circuit that is not well-described by the assumption of a simple resistor or diode, the value read is meaningless.  Large capacitors can cause transients that require running the cable tester in a slower data acquisition mode.  A future update might allow running at multiple output currents to check if the I-V curve scales linearly.  

When following up with a multimeter, keep in mind that diode voltage drops will differ depending on the current injected.  Also keep in mind that for some diodes 1 uA is below the leakage current, so you will see (different) voltages in both polarities on the cable tester that you might not see if your multimeter is using higher current levels.  Remember to try swapping multimeter leads to make sure what you measure behaves as you expect.

If you find the megatester reports a small excitation current (<100 nA instead of the 1200-1500 nA expected), verify that jumper J8 on the PCB is set to 2.5V (bridging pins 1 and 2). All other jumpers on the megatester should be unpopulated.
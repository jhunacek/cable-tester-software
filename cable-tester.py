#!/usr/bin/env python3

import datetime
import csv
import argparse
import time
import os
import sys
import numpy as np
from collections import OrderedDict

if sys.version_info.major != 3:
    sys.exit("This software is only supported on Python 3")
if sys.version_info.minor < 5:
    logging.warning("This software is expecting Python 3.5+, but version %i.%i was detected.  The software is attempting to run anyway, but if you run into issues your first step should be to update to the most current stable version of Python 3.x." % (sys.version_info.major, sys.version_info.minor))

try:
	import openpyxl
	if int(openpyxl.__version__[0]) < 2:
		print("Found openpyxl version " + str(openpyxl.__version__))
		sys.exit("Please upgrade openpyxl: sudo pip3 install openpyxl~=2.3")
	from openpyxl.styles import PatternFill, Color
except ImportError:
	sys.exit("Please install openpyxl: sudo pip3 install openpyxl~=2.3")

try: 
    from openpyxl.cell import get_column_letter
except ImportError:
    from openpyxl.utils import get_column_letter

try:
	import serial.abc # Sub-module in serial but not pyserial
	sys.exit("It appears you have serial installed, which conflicts with the required package pyserial.  Please remove serial and install pyserial.  (sudo pip3 uninstall serial; sudo pip3 install  pyserial~=3.2)")
except ImportError:
	# The offending package isn't present, good
	pass

try:
	import serial
except ImportError:
	sys.exit("Please install pyserial: sudo pip3 install pyserial~=3.2")

VALID_VERSIONS = ['mini', 'mega']

# Command line arguments
parser = argparse.ArgumentParser(
	formatter_class=argparse.RawDescriptionHelpFormatter,
	description='Automated cable tester control script.', 
	epilog='Simple example:\n   ./cable-tester.py -p pinmap/mega-mce-mux11-5mdm.txt\n ')
parser.add_argument('-c','--comport', type=str, nargs='?', default='/dev/ttyACM0', help='The serial port the cable tester is connected to (defaults to /dev/ttyACM0, should be of the form COM1 on Windows)')
parser.add_argument('-m','--mode', type=str, nargs='?', default=None, choices=['half', 'full', 'block-half', 'block-full', 'cross'], help='Chose to probe for non-redundant cross-shorts (half), to probe for all cross-shorts (full), to check only for shorts within each connector (block-half, block-full), or to probe from C0 to C1 on the Mini Cable Tester (cross).  Defaults to "half" or whatever is specified in the pinmap.')
parser.add_argument('-p','--pinmap', type=str, nargs='?', default=None, help='The path to a pin mapping file (defaults to None)')
parser.add_argument('-a','--ja', type=int, nargs='?', default=None, help='Number of JA connectors (100p MDM) to use on the Mega Cable Tester (up to 5, defaults to 5 or whatever is specified in the pinmap)')
parser.add_argument('-b','--jb', type=int, nargs='?', default=None, help='Number of JB connectors (37s MDM) to use on the Mega Cable Tester (up to 6, defaults to 0 or whatever is specified in the pinmap)')
parser.add_argument('-f','--filename', type=str, nargs='?', default=None, help='Output filename (defaults to current time)')

# They really should pass at least one argument.  If not, show the help.
if len(sys.argv) <= 1: 
	parser.print_help()
	exit()

args = parser.parse_args()

BAD_PKT_MSG = "An invalid packet was detected, and readout has stopped!  This can occur if the power or USB is interrupted.  Power cycle the cable tester and try again.  If this continues, try another USB port or a shorter cable."

# Interpret written numbers (possibly with SI prefixes) and return the
# resulting float
unit_prefix = {'E':1e18, 'P':1e15, 'T':1e12, 'G':1e9, 'M':1e6, 'k':1e3, 'd':1e-1, 'c':1e-2, 'm':1e-3, 'u':1e-6, 'n':1e-9, 'p':1e-12, 'f':1e-15, 'a':1e-18}
def interpret_number(str_val):
	try:
		if str_val[-1] in list(unit_prefix.keys()) and not str_val.endswith('inf'):
			return float(str_val[:-1]) * unit_prefix[str_val[-1]]
		else:
			return float(str_val)
	except ValueError:
		return None

# Load a pin remapping file and return a dictionary of the results.
# This lets you remap pins, provide expectations, and change runtime defaults.
def load_pinmap(fname):
	
	print("Loading pinmap...")
	
	pinmap = OrderedDict()
	expect = {'res':{}, 'diode':{}}
	num_read = 0
	default_param = {}
	
	if fname is None:
		return None, expect, default_param
		
	if not os.path.exists(fname):
		print("Pinmap file doesn't exist: " + str(fname))
		exit()
	
	with open(fname, 'r') as f:
		reader = csv.reader(f, delimiter=' ', quotechar='"')
		for row in reader:

			# Skip comments
			if len(row) < 1 or (row[0] == '#'):
				continue
			
			cmd = row[0]
			
			# Pin name
			if cmd == 'name':
				
				if len(row) != 3:
					print("Bad line in pinmap file: " + str(row))
					exit()
					
				mux_id = row[1]
				pinmap[mux_id] = row[2]
			
			# Expectation (ex. resistance range for pin combo)
			elif cmd in ['res', 'diode']:
				
				if len(row) not in [5,7]:
					print("Bad line in pinmap file: " + str(row))
					exit()
				
				pin_a = row[1]
				pin_b = row[2]
				xmin = interpret_number(row[3])
				xmax = interpret_number(row[4])
				
				# Diodes might need a reverse spec if reverse leakage
				# current is more then 1 uA
				if len(row) == 7:
					reverse_xmin = interpret_number(row[5])
					reverse_xmax = interpret_number(row[6])
				else:
					if cmd == 'diode':
						reverse_xmin = float('inf')
						reverse_xmax = float('inf')
					else:
						reverse_xmin = xmin
						reverse_xmax = xmax
				
				if any(x is None for x in [xmin, xmax, reverse_xmin, reverse_xmax]):
					print("Bad line in pinmap file: " + str(row))
					exit()					

				expect[cmd][(pin_a, pin_b)] = (xmin, xmax)
				expect[cmd][(pin_b, pin_a)] = (reverse_xmin, reverse_xmax)
				
				# Ignore resistance for diodes.  (Resistances have a
				# default open check, voltages don't.)
				if cmd == 'diode':
					expect['res'][(pin_a, pin_b)] = (float('-inf'), float('inf'))
					expect['res'][(pin_b, pin_a)] = (float('-inf'), float('inf'))
				
			
			# Change in defaults
			elif cmd in ['ja','jb']:
				try:
					default_param[cmd] = int(row[1])
				except ValueError:
					print("Bad line in pinmap file: " + str(row))
					exit()
			elif cmd in ['mode', 'hw']:
				default_param[cmd] = str(row[1])

	# Check our expectations
	for e in list(expect.values()):
		for (a,b) in list(e.keys()):
			if a not in list(pinmap.values()) or b not in list(pinmap.values()):
				print("Invalid expectation in pinmap file, pin combo (%s, %s) doesn't exist." % (a,b))
				exit()

	return pinmap, expect, default_param

class CableTester:
	
	def __init__(self, port, expected_ver = None):
		
		self.port = port
		self.gain = 0
		self.current_na = 0
		self.shorted_r = 0
		self.calib_received = False
		self.version = 'mini'
		self.resistance = {}
		self.r_correct = {}
		self.diode_v = {}
		self.pins = []
		self.last_b_pin = None
		self.expected_ver = expected_ver
		self.error_encountered = False
	
	# Read the resistance matrix
	def read(self, full_matrix, target_self, block_diag, num_ja, num_jb, fname_out_raw):
		
		self.start_time = time.time()
		
		num_ja = min(num_ja, 5)
		num_jb = min(num_jb, 6)
		num_ja = max(num_ja, 0)
		num_jb = max(num_jb, 0)
		
		print("Opening communications with " + str(args.comport) + "...")
		
		try:
			
			with serial.Serial(self.port, 230400, timeout=100) as ser:
				
				# Reboot in case we are in the middle of something
				ser.write(b'r')
				
				# Send a start command followed by the configuration
				print("Sending configuration to cable tester...")
				ser.write(b's') 
				ser.write(chr(full_matrix).encode())
				ser.write(chr(target_self).encode())
				ser.write(chr(block_diag).encode())
				ser.write(str(num_ja)[0].encode())
				ser.write(str(num_jb)[0].encode())
						
				print("Waiting for response...")
				
				with open(fname_out_raw, "w") as f:
					while(True):
						line = ser.readline().decode()
						f.write(line)					
						line = line.rstrip().split(',')
						if line[0] == '':
							continue
						if not self.process_packet(line):
							break
					
			print("Disconnecting from hardware")
			
		except serial.serialutil.SerialException as e:
			print(str(e))
			print("\nA serial port communication error occured.  Is the device plugged in?  Is the port correct?  (For example, the port could be /dev/ttyACM1 instead of 0, check /dev/ to see what is present when you plug the device in.  On Windows it will be COMx where x is an integer, check the device manager.  Use the -c flag to specify the port.)  Have you followed any relevant setup instructions for your OS (and rebooted if needed)?\n")
			exit()
			
	# Handle serial data.  Returns true if more packets are expected.
	def process_packet(self, pkt):
		
		# Firmware reports a failure
		if pkt[0] == 'ERROR':
			print(pkt)
			return False
		
		# Firmware booted
		elif pkt[0] == 'booted':
			return True
		
		# Debug message
		elif pkt[0] == 'debug':
			print(pkt)
			return True
		
		# Run time report, marks completion
		elif pkt[0] == 'time':
			print("Done, took %.1f seconds" % (int(pkt[1])/1000,))
			return False 
		
		# Calibration data, marks the start of valid data
		elif pkt[0] == 'calib':
			
			# Check HW version
			if len(pkt) > 3:
				self.version = pkt[3]
			else:
				self.version = 'mini'
			if self.version not in VALID_VERSIONS:
				print("Invalid cable tester version: " + str(self.version))
				exit()
			if self.expected_ver is not None and self.version != self.expected_ver:
				print("Pinmap file is expecting the wrong cable tester hardware: %s (found %s)" % (self.expected_ver, self.version))
				exit()
			print("Cable Tester Booted (version: %s)" % (self.version,))
			
			# Excitation current
			self.current_na = float(pkt[1])
						
			# Shorted mux value
			if self.version == "mini":
				offset_nV = float(pkt[2])
				self.shorted_r = offset_nV / self.current_na
			else:
				self.shorted_r = float(pkt[2])
				
			print("Calibration:")
			print("\tI = %.3f uA" % (self.current_na/1000,))
			print("\tR_shorted = %.3f ohms" % self.shorted_r)
			self.calib_received = True
			return True
		
		# Gain switch, tell the user
		elif pkt[0] == 'gain':
			self.gain = 2**(int(pkt[1]))
			if len(pkt) >= 3:
				if pkt[2] == 'open':
					print("Scanning for opens...")
					return True
				elif pkt[2] == 'icor':
					print("Checking leakage current for high-R combos...")
					return True
			print("Scanning with gain of " + str(self.gain) + "...") 
			return True
		
		# Current correction ratio
		elif pkt[0] == 'icor':
			
			# Attempt to extract data (a and b are the pin IDs)
			try:
				a, b, result = pkt[1:]
				if self.version == 'mini':
					a = "%03i" % (int(a)+1,)
					b = "%03i" % (int(b)+1,)
				result = float(result)
			except ValueError:
				print("Error, bad packet:", pkt)
				print(BAD_PKT_MSG)
				sys.exit()
			
			# The mini cable tester calls GND pin 128
			if a == '129' and self.version == 'mini':
				a = 'GND'
			
			# The current correction factor should be close to 1 for 
			# small R, and close to .8 for large R.  This is caused by
			# leakage current (in the multiplexers probably).
			if (result > 0.5 and result < 1.1):
				self.r_correct[(a,b)] = result
			else:
				self.error_encountered = True
				print("Ignoring unrealistic current correction factor (%s)" % (" ".join(pkt),))
			return True
			
		# We didn't get a named packet, so it is a data packet
		
		if not self.calib_received:
			#~ # Give the HW a moment to reboot if it was already running.
			#~ # Ignore any data it sends until calibration is received.
			#~ if (time.time() - self.start_time > 3):
				print("Please reset the cable tester, it seems to already be taking data.")
				exit()
			#~ else:
				#~ return True
		
		# Attempt to extract data (a and b are the pin IDs)
		try:
			a, b, result = pkt
			if self.version == 'mini':
					a = "%03i" % (int(a)+1,)
					b = "%03i" % (int(b)+1,)
			result = float(result)
		except ValueError:
			print("Error, bad packet:", pkt)
			print(BAD_PKT_MSG)
			sys.exit()
		
		# The mini cable tester calls GND pin 128
		if a == '129' and self.version == 'mini':
			a = 'GND'
		
		if self.version == 'mini':
			# Result is a voltage from a constant current source.  Diode
			# voltages are known directly, resistances are computed from
			# the excitation current.
			nv = result
			r = nv / self.current_na - self.shorted_r
		else:
			# Result is a pre-computed resistance from a constant voltage
			# with a 2 Mohm series resistor.  If the sensor is actually
			# a diode, we compute its voltage from the excitation
			# current.
			r = result
			nv = self.current_na * (r + self.shorted_r)
		
		r = max(r, 0)		
		self.resistance[(a,b)] = r
		self.diode_v[(a,b)] = nv * 1e-9
		
		if a not in self.pins:
			self.pins.append(a)
		if b not in self.pins:
			self.pins.append(b)
		self.pins.sort()
		
		# Convert to pinmap names if relevant
		if pinmap is not None:
			a = pinmap.get(a, None)
			b = pinmap.get(b, None)
			if a is None or b is None:
				return True
		
		# Show results live
		if np.isfinite(r):
			print("\t%s to %s is %.1f ohms" % (a, b, r))
		elif self.version != 'mini':
			# Show occasional messages so the user doesn't think we froze.
			# The mini is small enough that we don't need it.
			if a == 'GND':
				if self.last_b_pin != a:
					self.last_b_pin = a
					print("\tScanning combos with pin %s..." % (a,))
			elif b != self.last_b_pin:
				self.last_b_pin = b
				print("\tScanning combos with pin %s..." % (b,))
		
		return True
			
						
if __name__ == '__main__':
	
	folder_out = 'output'
	if not os.path.exists(folder_out):
		os.makedirs(folder_out)
	start_time = time.time()
	
	fname_prefix = "cable-tester-" + str(int(start_time))
	if args.filename is not None:
		fname_prefix = str(args.filename)
	
	fname_out = os.path.join(folder_out, fname_prefix + ".xlsx")
	fname_out_raw = os.path.join(folder_out, "debug-" + fname_prefix + ".txt")
	
	if os.path.exists(fname_out):
		print("Output file name already exists")
		exit()
	
	r_lim = [100, 1000, 10000, 100000, 1000000]
	colors = ['FFFF00FF', 'FFFF0000', 'FFFFFF00', 'FF00FF00', 'FF00FFFF']
	r_style = [PatternFill(patternType='solid', fgColor=Color(c)) for c in colors]
	fail_style = PatternFill(patternType='solid', fgColor=Color('FFEF9A9A'))
	pass_style = PatternFill(patternType='solid', fgColor=Color('FFA5D6A7'))
	
	pinmap, expect, default_param = load_pinmap(args.pinmap)
	
	# Load in defaults from the pinmap file if needed
	if args.mode is None:
		args.mode = default_param.get('mode', 'half')
	if args.ja is None:
		args.ja = default_param.get('ja', 5)
	if args.jb is None:
		args.jb = default_param.get('jb', 0)
	
	# Convert the mode into params sent to the cable tester
	if args.mode == 'half':
		full_matrix = False
		target_self = True
		block_diag = False
	elif args.mode == 'full':
		full_matrix = True
		target_self = True
		block_diag = False
	elif args.mode == 'block-half':
		full_matrix = False
		target_self = True
		block_diag = True
	elif args.mode == 'block-full':
		full_matrix = True
		target_self = True
		block_diag = True
	elif args.mode == 'cross':
		full_matrix = True
		target_self = False
		block_diag = False
	else:
		print("Invalid mode: " + str(args.mode))
		exit()
	
	wb = openpyxl.Workbook()

	# Create the workbook sheets
	if pinmap is not None:
		ws_expect = wb.active
		ws_expect.title = "Expectations"
		ws_expect.freeze_panes = ws_expect['A6']
		ws_main = wb.create_sheet(title="Matrix")
		ws_main.freeze_panes = ws_main['B2']
		ws_raw_r = wb.create_sheet()
	else:
		ws_raw_r = wb.active
	ws_raw_r.title = "Raw (Resistor)"
	ws_raw_r.freeze_panes = ws_raw_r['B2']
	ws_raw_v = wb.create_sheet(title="Raw (Diode)")
	ws_raw_v.freeze_panes = ws_raw_v['B2']
	ws_meta = wb.create_sheet(title="Metadata")
	ws_meta.column_dimensions[get_column_letter(1)].width = 20
	ws_meta.column_dimensions[get_column_letter(2)].width = 50
	
	# Save config
	ws_meta['A1'] = "Date:"
	ws_meta['B1'] = str(datetime.datetime.now())
	ws_meta['A2'] = "Full Matrix:"
	ws_meta['B2'] = str(full_matrix)
	ws_meta['A3'] = "Target Self:"
	ws_meta['B3'] = str(target_self)
	ws_meta['A4'] = "Block Diag:"
	ws_meta['B4'] = str(block_diag)
	ws_meta['A5'] = "Num JA:"
	ws_meta['B5'] = str(args.ja)
	ws_meta['A6'] = "Num JB:"
	ws_meta['B6'] = str(args.jb)
	ws_meta['A7'] = "Pinmap:"
	ws_meta['B7'] = str(args.pinmap)
					
	# Do the measurement
	expected_ver = default_param.get('hw', None)
	ct = CableTester(args.comport, expected_ver = expected_ver)
	ct.read(full_matrix, target_self, block_diag, args.ja, args.jb, fname_out_raw)
	print("Saving results...")
	
	# Save calibration
	ws_meta['A8'] = "Current [nA]:"
	ws_meta['B8'] = str(ct.current_na)
	ws_meta['A9'] = "Shorted R [Ohms]:"
	ws_meta['B9'] = str(ct.shorted_r)
	ws_meta['A10'] = "HW Version:"
	ws_meta['B10'] = str(ct.version)
	
	# Show GND as the first pin
	pins_raw_b = ct.pins
	if "GND" in pins_raw_b:
		pins_raw_b.remove("GND")
	pins_raw_a = ["GND"] + pins_raw_b
	if pinmap is not None:
		pins_main_b = list(pinmap.keys())
		if "GND" in pins_main_b:
			pins_main_b.remove("GND")
		else:
			pinmap["GND"] = "GND"
		pins_main_a = ["GND"] + pins_main_b

	# Create the headers for the main result file
	if pinmap is not None:
		for i in range(len(pins_main_a)):
			ws_main.cell(column=i+2, row=1, value=pinmap[pins_main_a[i]])
		for j in range(len(pins_main_b)):
			ws_main.cell(column=1, row=j+2, value=pinmap[pins_main_b[j]])
					
		# Wider columns
		for col in range(len(pins_main_a)+1):
			ws_main.column_dimensions[get_column_letter(col+1)].width = 15
		
		for col in range(4,9):
			ws_expect.column_dimensions[get_column_letter(col+1)].width = 15
		for col in range(4):
			ws_expect.column_dimensions[get_column_letter(col+1)].width = 20
		ws_expect.column_dimensions[get_column_letter(10)].width = 50
			
	# Create the headers for the raw mux data
	for i in range(len(pins_raw_a)):
		ws_raw_r.cell(column=i+2, row=1, value=pins_raw_a[i])
		ws_raw_v.cell(column=i+2, row=1, value=pins_raw_a[i])
	for j in range(len(pins_raw_b)):
		ws_raw_r.cell(column=1, row=j+2, value=pins_raw_b[j])
		ws_raw_v.cell(column=1, row=j+2, value=pins_raw_b[j])
	
	# Populate the spreadsheet
	for i in range(len(pins_raw_a)):
		for j in range(len(pins_raw_b)):

			r_cor = ct.r_correct.get((pins_raw_a[i],pins_raw_b[j]), None)
			res = ct.resistance.get((pins_raw_a[i],pins_raw_b[j]), None)
			v = ct.diode_v.get((pins_raw_a[i],pins_raw_b[j]), None)
			
			if res is None or v is None:
				continue
				
			# Apply leakage current correction if needed
			if r_cor is not None:
				res /= r_cor
			
			# Transpose elements as needed for easier half-matrix viewing
			if (i > j or full_matrix or pins_raw_a[i] == "GND"):
				col = i
				row = j
			else:
				col = j+1
				row = i-1
							
			ws_raw_r.cell(column=col+2, row=row+2, value="%.1f" % res)
			ws_raw_v.cell(column=col+2, row=row+2, value="%.5g" % v)
			
			# Determine if this is in the main result page, add it if so
			in_main = False
			if (pinmap is not None):
				if (pins_raw_a[i] in pins_main_a) and (pins_raw_b[j] in pins_main_b):
					in_main = True				
					main_col = pins_main_a.index(pins_raw_a[i])
					main_row = pins_main_b.index(pins_raw_b[j])
										
					# Transpose elements as needed for easier half-matrix viewing
					if (main_row >= main_col) and not full_matrix and pins_raw_a[i] != "GND":
						main_col, main_row = main_row+1, main_col-1
						
					ws_main.cell(column=main_col+2, row=main_row+2, value="%.1f" % res)
			
			# Color code the cells
			for rrr in range(len(r_lim)):
				if res <= r_lim[rrr]:
					ws_raw_r.cell(column=col+2, row=row+2).fill = r_style[rrr]
					if in_main:
						ws_main.cell(column=main_col+2, row=main_row+2).fill = r_style[rrr]
					break
	
	# Check expectations
	if pinmap is not None:
		
		expect_fail = []
		expect_pass = []
		
		for a in pins_main_a:
			for b in pins_main_b:
				
				if (a,b) not in ct.resistance:
					continue
				
				a_name = pinmap[a]
				b_name = pinmap[b]

				# Get the expecation for this combo
				e_r = expect['res'].get((a_name,b_name), None)
				e_v = expect['diode'].get((a_name,b_name), None)
				
				notes = ""
				
				# Check resistance
				r = ct.resistance[(a,b)]
				r_cor = ct.r_correct.get((a,b), None)
				if r_cor is not None:
					r /= r_cor
				default_open = False
				if e_r is not None:
					rmin, rmax = e_r
				else:
					# Default to assuming that pins with the same name
					# must be shorted and all others must be open
					if (a_name == b_name):
						rmin, rmax = -10, 100
					else:
						default_open = True
						rmin, rmax = float('inf'), float('inf')
				
				# Adjust expectations for ground short if needed,
				# using the short from the positive side.
				# Note: for two pins with no cross short, a ground short
				# on the low side doesn't cause a ghost.  I reverse the
				# mux (in firmware) in these cases to reduce ghosting.
				# This is why some combos do not show ghosts.
				gnd_b = ct.resistance[('GND',b)]
				if np.isfinite(gnd_b) and gnd_b >= 0 and a != 'GND':
					notes = "Adjusted expectations for EXC+ ground short ghost"
					default_open = False
					if gnd_b > 100:
						# The ground short is large enough that we can
						# try to detect smaller cross shorts
						if rmin > 0:
							rmin = round(1/(1/rmin + 1/(0.7*gnd_b)))
						if rmax > 0 and np.isfinite(rmax): # Leave as +inf if already +inf, that happens sometimes
							rmax = round(1/(1/rmax + 1/(1.3*gnd_b)))
					else:
						# The ground short is so low, there's no hope
						rmin = -10
						rmax = 200
						
				e = [a_name,b_name,str(a),str(b),'Resistance',rmin,rmax,"%0.2f"%r,notes]
				if r < rmin or r > rmax:
					expect_fail.append(e)
				else:
					if not default_open:
						expect_pass.append(e)
					
				# Check diode voltage only if explicitly told to
				if e_v is not None:
					vmin, vmax = e_v
					v = ct.diode_v[(a,b)]
					e = [a_name,b_name,str(a),str(b),'Voltage',vmin,vmax,"%0.5g"%v,notes]
					if v < vmin or v > vmax:
						expect_fail.append(e)
					else:
						expect_pass.append(e)
		
		# Put the passed ground ghosts at the bottom
		expect_pass = sorted(expect_pass, key=lambda x: (x[-1],x[0],x[1],x[2]))
		expect_fail.sort()	
				
		# Print out the results
		ws_expect.cell(column=1, row=1, value="Failed (red) and passed (green) expectations are shown below.")
		ws_expect.cell(column=1, row=2, value="Connector pins numbers are 1-indexed, connector labels (A0, B3, etc.) are 0-indexed.")
		ws_expect.cell(column=1, row=3, value="The full resistance matrix can be found in the next tab.  Colors indicate order of magnitude.")
		
		pos = 5
		ws_expect.cell(column=1, row=pos, value="Pin A Name (EXC-)")
		ws_expect.cell(column=2, row=pos, value="Pin B Name (EXC+)")
		ws_expect.cell(column=3, row=pos, value="Pin A HW")
		ws_expect.cell(column=4, row=pos, value="Pin B HW")
		ws_expect.cell(column=5, row=pos, value="Type")
		ws_expect.cell(column=6, row=pos, value="Min Val")
		ws_expect.cell(column=7, row=pos, value="Max Val")
		ws_expect.cell(column=8, row=pos, value="Measured")
		ws_expect.cell(column=9, row=pos, value="Status")
		ws_expect.cell(column=10, row=pos, value="Notes")
		for e in expect_fail:
			e.insert(-1,'FAIL!')
			for i in range(len(e)):
				ws_expect.cell(column=i+1, row=pos+1, value=str(e[i]))
				ws_expect.cell(column=i+1, row=pos+1).fill = fail_style
			pos += 1
		ws_expect.cell(column=1, row=pos+1, value="(Not listing passed expectations for open pin combos.)")
		pos += 1
		for e in expect_pass:
			e.insert(-1,'Pass')
			for i in range(len(e)):
				ws_expect.cell(column=i+1, row=pos+1, value=str(e[i]))
				ws_expect.cell(column=i+1, row=pos+1).fill = pass_style
			pos += 1
			
		print(str(len(expect_pass)) + " expectations passed")
		print(str(len(expect_fail)) + " expectations failed")
	
	# Save the file
	wb.save(filename = fname_out)
	print("Saved to " + fname_out)
	
	# Not needed currently
	#if ct.error_encountered:
	#	print("An minor error occured during this test and a correction was applied.  You probably have nothing to worry about if the results don't look catastrophic.  Please send the results and the debug file to Jon Hunacek.")
